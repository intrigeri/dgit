Format: 1.0
Source: pari-extra
Binary: pari-extra
Architecture: all
Version: 3-2~dummy1
Maintainer: Bill Allombert <ballombe@debian.org>
Standards-Version: 3.9.2.0
Build-Depends: debhelper (>= 5), package-does-not-exist
Package-List: 
 pari-extra deb math optional
Checksums-Sha1: 
 ff281e103ab11681324b0c694dd3514d78436c51 121 pari-extra_3.orig.tar.gz
 810c43d186ad2552d65949acf4a065fcfc823636 2484 pari-extra_3-2~dummy1.diff.gz
Checksums-Sha256: 
 ac1ef39f9da80b582d1c0b2adfb09b041e3860ed20ddcf57a0e922e3305239df 121 pari-extra_3.orig.tar.gz
 41f47f24df7f50555f43549bd8377cce046750d29f69903e04b7fbfe396a0a73 2484 pari-extra_3-2~dummy1.diff.gz
Files: 
 76bcf03be979d3331f9051aa88439b8b 121 pari-extra_3.orig.tar.gz
 eff09e2ace409a150646c4994f17f800 2484 pari-extra_3-2~dummy1.diff.gz
