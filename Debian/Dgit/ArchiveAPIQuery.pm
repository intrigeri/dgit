# -*- perl -*-

package Debian::Dgit::ArchiveAPIQuery;

BEGIN {
    use Exporter;
    @ISA = qw(Exporter);
    @EXPORT = qw(archive_api_query_curl);
}

use Debian::Dgit;
use Debian::Dgit::I18n;
use WWW::Curl::Easy;

sub archive_api_query_curl ($$) {
    my ($url, $cacert) = @_;

    my @curl_opts = (
        CURLOPT_SSL_VERIFYHOST,  2,
        CURLOPT_SSL_VERIFYPEER,  1,
        CURLOPT_PROTOCOLS,       CURLPROTO_HTTPS,
        CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTPS,
    );
    if ($cacert) {
        push @curl_opts, CURLOPT_CAINFO, $cacert;
        push @curl_opts, CURLOPT_CAPATH, '';
    }
    my %curl_opts = @curl_opts;

    my $curl  = WWW::Curl::Easy->new;
    my $response_body;
    $curl->setopt(CURLOPT_URL, $url);
    $curl->setopt(CURLOPT_WRITEDATA, \$response_body);
    while (my ($k, $v) = each(%curl_opts)) {
        $curl->setopt($k, $v);
    }

    my $retcode = $curl->perform;
    $retcode == 0 or
        fail f_ "Could not download '%s', request failed (%s): %s\n",
                $url, $curl->strerror($retcode), $curl->errbuf;

    return ($curl->getinfo(CURLINFO_HTTP_CODE), $response_body);
}

1;
