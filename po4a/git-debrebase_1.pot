# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-03-01 16:59+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: =head1
#: ../dgit.1:3 ../dgit.7:2 ../dgit-user.7.pod:1 ../dgit-nmu-simple.7.pod:1
#: ../dgit-maint-native.7.pod:1 ../dgit-maint-merge.7.pod:1
#: ../dgit-maint-gbp.7.pod:1 ../dgit-maint-debrebase.7.pod:1
#: ../dgit-downstream-dsc.7.pod:1 ../dgit-sponsorship.7.pod:1
#: ../git-debrebase.1.pod:1 ../git-debrebase.5.pod:1
#, no-wrap
msgid "NAME"
msgstr ""

#. type: =head1
#: ../dgit.1:496 ../git-debrebase.1.pod:462
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: =head1
#: ../dgit.1:1470 ../dgit.7:23 ../dgit-user.7.pod:447
#: ../dgit-nmu-simple.7.pod:137 ../dgit-maint-native.7.pod:126
#: ../dgit-maint-merge.7.pod:491 ../dgit-maint-gbp.7.pod:136
#: ../dgit-maint-debrebase.7.pod:747 ../dgit-downstream-dsc.7.pod:352
#: ../dgit-sponsorship.7.pod:321 ../git-debrebase.1.pod:619
#: ../git-debrebase.5.pod:678
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: =head1
#: ../dgit-user.7.pod:5 ../dgit-maint-native.7.pod:5
#: ../dgit-maint-merge.7.pod:5 ../dgit-maint-gbp.7.pod:5
#: ../dgit-maint-debrebase.7.pod:5 ../dgit-downstream-dsc.7.pod:5
#: ../git-debrebase.1.pod:10 ../git-debrebase.5.pod:5
msgid "INTRODUCTION"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:3
msgid "git-debrebase - delta queue rebase tool for Debian packaging"
msgstr ""

#. type: =head1
#: ../git-debrebase.1.pod:5
msgid "SYNOPSYS"
msgstr ""

#. type: verbatim
#: ../git-debrebase.1.pod:7
#, no-wrap
msgid ""
" git-debrebase [<options...>] [-- <git-rebase options...>]\n"
" git-debrebase [<options...>] <operation> [<operation options...>\n"
"\n"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:12 ../git-debrebase.5.pod:7
msgid ""
"git-debrebase is a tool for representing in git, and manpulating, Debian "
"packages based on upstream source code."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:16
msgid ""
"This is the command line reference.  Please read the tutorial L<dgit-maint-"
"debrebase(7)>.  For background, theory of operation, and definitions see "
"L<git-debrebase(5)>."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:22
msgid ""
"You should read this manpage in conjunction with L<git-debrebase(5)/"
"TERMINOLOGY>, which defines many important terms used here."
msgstr ""

#. type: =head1
#: ../git-debrebase.1.pod:26
msgid "PRINCIPAL OPERATIONS"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:30
msgid "git-debrebase [-- <git-rebase options...>]"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:32
msgid "git-debrebase [-i <further git-rebase options...>]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:34
msgid ""
"Unstitches and launders the branch.  (See L</UNSTITCHING AND LAUNDERING> "
"below.)"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:37
msgid ""
"Then, if any git-rebase options were supplied, edits the Debian delta queue, "
"using git-rebase, by running"
msgstr ""

#. type: verbatim
#: ../git-debrebase.1.pod:41
#, no-wrap
msgid ""
"    git rebase <git-rebase options> <breakwater-tip>\n"
"\n"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:43
msgid ""
"Do not pass a base branch argument: git-debrebase will supply that.  Do not "
"use --onto, or --fork-point.  Useful git-rebase options include -i and --"
"autosquash."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:48
msgid ""
"If git-rebase stops for any reason, you may git-rebase --abort, --continue, "
"or --skip, as usual.  If you abort the git-rebase, the branch will still "
"have been laundered, but everything in the rebase will be undone."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:54
msgid ""
"The options for git-rebase must either start with C<-i>, or be prececded by "
"C<-->, to distinguish them from options for git-debrebase."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:58
msgid ""
"It is hazardous to use plain git-rebase on a git-debrebase branch, because "
"git-rebase has a tendency to start the rebase too far back in history, and "
"then drop important commits.  See L<git-debrebase(5)/ILLEGAL OPERATIONS>"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:64
msgid "git-debrebase status"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:66
msgid ""
"Analyses the current branch, both in terms of its contents, and the refs "
"which are relevant to git-debrebase, and prints a human-readable summary."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:71
msgid ""
"Please do not attempt to parse the output; it may be reformatted or "
"reorganised in the future.  Instead, use one of the L<UNDERLYING AND "
"SUPPLEMENTARY OPERATIONS> described below."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:77
msgid "git-debrebase conclude"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:79
msgid ""
"Finishes a git-debrebase session, tidying up the branch and making it fast "
"forward again."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:82
msgid ""
"Specifically: if the branch is unstitched, launders and restitches it, "
"making a new pseudomerge.  Otherwise, it is an error, unless --noop-ok."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:88
msgid "git-debrebase quick"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:90
msgid ""
"Unconditionally launders and restitches the branch, consuming any ffq-prev "
"and making a new pseudomerge."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:94
msgid "If the branch is already laundered and stitched, does nothing."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:96
msgid "git-debrebase prepush [--prose=<for commit message>]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:98
msgid "If the branch is unstitched, stitches it, consuming ffq-prev."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:102
msgid ""
"This is a good command to run before pushing to a git server.  You should "
"consider using B<conclude> instead, because that launders the branch too."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:106
msgid "git-debrebase stitch [--prose=<for commit message>]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:108
msgid "Stitches the branch, consuming ffq-prev."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:111
msgid "If there is no ffq-prev, it is an error, unless --noop-ok."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:113
msgid "You should consider using B<prepush> or B<conclude> instead."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:115
msgid "git-debrebase scrap"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:117
msgid ""
"Throws away all the work since the branch was last stitched.  This is done "
"by rewinding you to ffq-prev."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:120
msgid "If you are in the middle of a git-rebase, will abort that too."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:122
msgid "git-debrebase new-upstream <new-version> [<upstream-details>...]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:124
msgid "Rebases the delta queue onto a new upstream version.  In detail:"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:127
msgid ""
"Firstly, checks that the proposed rebase seems to make sense: It is a snag "
"unless the new upstream(s)  are fast forward from the previous upstream(s)  "
"as found in the current breakwater anchor.  And, in the case of a multi-"
"piece upstream (a multi-component upstream, in dpkg-source terminology), if "
"the pieces are not in the same order, with the same names."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:135
msgid "If all seems well, unstitches and launders the branch."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:137
msgid ""
"Then, generates (in a private working area)  a new anchor merge commit, on "
"top of the breakwater tip, and on top of that a commit to update the version "
"number in debian/changelog."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:145
msgid "Finally, starts a git-rebase of the delta queue onto these new commits."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:149
msgid ""
"That git-rebase may complete successfully, or it may require your "
"assistance, just like a normal git-rebase."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:153
msgid ""
"If you git-rebase --abort, the whole new upstream operation is aborted, "
"except for the laundering."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:157
msgid ""
"<new-version> may be a whole new Debian version, including revision, or just "
"the upstream part, in which case -1 will be appended to make the new Debian "
"version."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:163
msgid "The <upstream-details> are, optionally, in order:"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:167
msgid "<upstream-commit-ish>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:169
msgid ""
"The new upstream branch (or commit-ish).  The default is to look for one of "
"these tags, in this order: U vU upstream/U; where U is the new upstream "
"version.  (This is the same algorithm as L<git-deborig(1)>.)"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:175
msgid ""
"It is a snag if the upstream contains a debian/ directory; if forced to "
"proceed, git-debrebase will disregard the upstream's debian/ and take (only) "
"the packaging from the current breakwater."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:180
msgid "<piece-name> <piece-upstream-commit-ish>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:182
msgid "Specifies that this is a multi-piece upstream.  May be repeated."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:185
msgid ""
"When such a pair is specified, git-debrebase will first combine the pieces "
"of the upstream together, and then use the result as the combined new "
"upstream."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:190
msgid ""
"For each <piece-name>, the tree of the <piece-upstream-commit-ish> becomes "
"the subdirectory <piece-name> in the combined new upstream (supplanting any "
"subdirectory that might be there in the main upstream branch)."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:197
msgid ""
"<piece-name> has a restricted syntax: it may contain only ASCII "
"alphanumerics and hyphens."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:200
msgid ""
"The combined upstream is itself recorded as a commit, with each of the "
"upstream pieces' commits as parents.  The combined commit contains an "
"annotation to allow a future git-debrebase new upstream operation to make "
"the coherency checks described above."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:206
msgid "<git-rebase options>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:208
msgid "These will be passed to git rebase."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:210
msgid ""
"If the upstream rebase is troublesome, -i may be helpful.  As with plain git-"
"debrebase, do not specify a base, or --onto, or --fork-point."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:216
msgid ""
"If you are planning to generate a .dsc, you will also need to have, or "
"generate, actual orig tarball(s), which must be identical to the rev-"
"spec(s)  passed to git-debrebase.  git-debrebase does not concern itself "
"with source packages so neither helps with this, nor checks it.  L<git-"
"deborig(1)>, L<git-archive(1)>, L<dgit(1)> and L<gbp-import-orig(1)> may be "
"able to help."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:227
msgid "git-debrebase make-patches [--quiet-would-amend]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:229
msgid ""
"Generate patches in debian/patches/ representing the changes made to "
"upstream files."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:232
msgid ""
"It is not normally necessary to run this command explicitly.  When uploading "
"to Debian, dgit and git-debrebase will cooperate to regenerate patches as "
"necessary.  When working with pure git remotes, the patches are not needed."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:239
msgid ""
"Normally git-debrebase make-patches will require a laundered branch.  (A "
"laundered branch does not contain any patches.)  But if there are already "
"some patches made by git-debrebase make-patches, and all that has happened "
"is that more changes to upstream files have been committed, running it again "
"can add the missing patches."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:248
msgid ""
"If the patches implied by the current branch are not a simple superset of "
"those already in debian/patches, make-patches will fail with exit status 7, "
"and an error message.  (The message can be suppressed with --quiet-would-"
"amend.)  If the problem is simply that the existing patches were not made by "
"git-debrebase, using dgit quilt-fixup instead should succeed."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:257
msgid "git-debrebase convert-from-unapplied [<upstream-commit-ish>]"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:259
msgid "git-debrebase convert-from-gbp [<upstream-commit-ish>]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:261
msgid "Converts any of the following into a git-debrebase interchange branch:"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:267
msgid "a gbp patches-unapplied branch (but not a gbp pq patch-queue branch)"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:271
msgid ""
"a patches-unapplied git packaging branch containing debian/patches, as used "
"with quilt"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:276
msgid ""
"a git branch for a package which has no Debian delta - ie where upstream "
"files are have not been modified in Debian, so there are no patches"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:282
msgid "(These two commands operate identically and are simply aliases.)"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:284
msgid ""
"The conversion is done by generating a new anchor merge, converting any "
"quilt patches as a delta queue, and dropping the patches from the tree."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:288
msgid ""
"The upstream commit-ish should correspond to the upstream branch or tag, if "
"there is one.  It is a snag if it is not an ancestor of HEAD, or if the "
"history between the upstream and HEAD contains commits which make changes to "
"upstream files.  If it is not specified, the same algorithm is used as for "
"git-debrebase new-upstream."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:296
msgid ""
"It is also a snag if the specified upstream has a debian/ subdirectory.  "
"This check exists to detect certain likely user errors, but if this "
"situation is true and expected, forcing it is fine."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:302
msgid ""
"git-debrebase will try to look for the dgit archive view of the most recent "
"release, and if it finds it will make a pseduomerge so that your new git-"
"debrebase view is appropriately fast forward."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:307
msgid ""
"The result is a well-formed git-debrebase interchange branch.  The result is "
"also fast-forward from the original branch."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:310
msgid ""
"It is a snag if the new branch looks like it will have diverged, just as for "
"a laundering/unstitching call to git-debrebase; See L</Establish the current "
"branch's ffq-prev>, below."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:314
msgid ""
"Note that it is dangerous not to know whether you are dealing with a (gbp) "
"patches-unapplied branch containing quilt patches, or a git-debrebase "
"interchange branch.  At worst, using the wrong tool for the branch format "
"might result in a dropped patch queue!"
msgstr ""

#. type: =head1
#: ../git-debrebase.1.pod:323
msgid "UNDERLYING AND SUPPLEMENTARY OPERATIONS"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:327
msgid "git-debrebase breakwater"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:329
msgid ""
"Prints the breakwater tip commitid.  If your HEAD branch is not fully "
"laundered, prints the tip of the so-far-laundered breakwater."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:333
msgid "git-debrebase anchor"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:335
msgid "Prints the breakwater anchor commitid."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:337
msgid "git-debrebase analyse"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:339
msgid ""
"Walks the history of the current branch, most recent commit first, back "
"until the most recent anchor, printing the commit object id, and commit type "
"and info (ie the semantics in the git-debrebase model)  for each commit."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:347
msgid "git-debrebase record-ffq-prev"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:349
msgid ""
"Establishes the current branch's ffq-prev, as discussed in L</UNSTITCHING "
"AND LAUNDERING>, but does not launder the branch or move HEAD."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:353
msgid ""
"It is an error if the ffq-prev could not be recorded.  It is also an error "
"if an ffq-prev has already been recorded, unless --noop-ok."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:357
msgid "git-debrebase launder-v0"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:359
msgid ""
"Launders the branch without recording anything in ffq-prev.  Then prints "
"some information about the current branch.  Do not use this operation; it "
"will be withdrawn soon."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:364
msgid "git-debrebase convert-to-gbp"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:366
msgid ""
"Converts a laundered branch into a gbp patches-unapplied branch containing "
"quilt patches.  The result is not fast forward from the interchange branch, "
"and any ffq-prev is deleted."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:371
msgid ""
"This is provided mostly for the test suite and for unusual situations.  It "
"should only be used with care and with a proper understanding of the "
"underlying theory."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:376
msgid ""
"Be sure to not accidentally treat the result as a git-debrebase branch, or "
"you will drop all the patches!"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:380
msgid "git-debrebase convert-from-dgit-view [<convert-options>] [upstream]"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:382
msgid ""
"Converts any dgit-compatible git branch corresponding to a (possibly "
"hypothetical) 3.0 quilt dsc source package into a git-debrebase-compatible "
"branch."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:386
msgid ""
"This operation should not be used if the branch is already in git-debrebase "
"form.  Normally git-debrebase will refuse to continue in this case (or "
"silently do nothing if the global --noop-ok option is used)."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:391
msgid ""
"Some representation of the original upstream source code will be needed.  If "
"I<upstream> is supplied, that must be a suitable upstream commit.  By "
"default, git-debrebase will look first for git tags (as for new-upstream), "
"and then for orig tarballs which it will ask dgit to process."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:397
msgid ""
"The upstream source must be exactly right and all the patches in debian/"
"patches must be up to date.  Applying the patches from debian/patches to the "
"upstream source must result in exactly your HEAD."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:402
msgid ""
"The output is laundered and stitched.  The resulting history is not "
"particularly pretty, especially if orig tarball(s) were imported to produce "
"a synthetic upstream commit."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:407
msgid ""
"The available convert-options are as follows.  (These must come after "
"convert-from-dgit-view.)"
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:412
msgid "--[no-]diagnose"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:414
msgid ""
"Print additional error messages to help diagnose failure to find an "
"appropriate upstream.  --no-diagnose is the default."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:418
msgid "--build-products-dir"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:420
msgid ""
"Directory to look in for orig tarballs.  The default is the git config "
"option dgit.default.build-products-dir or failing that, C<..>.  Passed on to "
"dgit, if git-debrebase invokes dgit."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:426
msgid "--[no-]origs"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:428
msgid ""
"Whether to try to look for or use any orig tarballs.  --origs is the default."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:431
msgid "--[no-]tags"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:433
msgid ""
"Whether to try to look for or use any upstream git tags.  --tags is the "
"default."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:436
msgid "--always-convert-anyway"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:438
msgid ""
"Perform the conversion operation, producing unpleasant extra history, even "
"if the branch seems to be in git-debrebase form already.  This should not be "
"done unless necessary, and it should not be necessary."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:446
msgid "git-debrebase forget-was-ever-debrebase"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:448
msgid ""
"Deletes the ffq-prev and debrebase-last refs associated with this branch, "
"that git-debrebase and dgit use to determine whether this branch is managed "
"by git-debrebase, and what previous head may need to be stitched back in."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:454
msgid ""
"This can be useful if you were just playing with git-debrebase, and have "
"used git-reset --hard to go back to a commit before your experiments."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:458
msgid "Do not use this if you expect to run git-debrebase on the branch again."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:464
msgid ""
"This section documents the general options to git-debrebase (ie, the ones "
"which immediately follow git-debrebase or git debrebase on the command "
"line).  Individual operations may have their own options which are docuented "
"under each operation."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:476
msgid "-f<snag-id>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:478
msgid "Turns snag(s) with id <snag-id> into warnings."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:480
msgid ""
"Some troublesome things which git-debrebase encounters are B<snag>s.  (The "
"specific instances are discussed in the text for the relevant operation.)"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:485
msgid ""
"When a snag is detected, a message is printed to stderr containing the snag "
"id (in the form C<-f<snag-idE<gt>>), along with some prose."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:490
msgid ""
"If snags are detected, git-debrebase does not continue, unless the relevant -"
"f<snag-id> is specified, or --force is specified."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:494
msgid "--force"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:496
msgid "Turns all snags into warnings.  See the -f<snag-id> option."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:499
msgid ""
"Do not invoke git-debrebase --force in scripts and aliases; instead, specify "
"the particular -f<snag-id> for expected snags."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:502
msgid "--noop-ok"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:504
msgid ""
"Suppresses the error in some situations where git-debrebase does nothing, "
"because there is nothing to do."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:508
msgid ""
"The specific instances are discussed in the text for the relvant operation."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:511
msgid "--anchor=<commit-ish>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:513
msgid ""
"Treats <commit-ish> as an anchor.  This overrides the usual logic which "
"automatically classifies commits as anchors, pseudomerges, delta queue "
"commits, etc."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:517
msgid ""
"It also disables some coherency checks which depend on metadata extracted "
"from its commit message, so it is a snag if <commit-ish> is the anchor for "
"the previous upstream version in git-debrebase new-upstream operations."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:524
msgid "--dgit=<program>"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:526
msgid ""
"Run <program>, instead of dgit from PATH, when invocation of dgit is "
"necessary.  This is provided mostly for the benefit of the test suite."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:530
msgid "-D"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:532
msgid "Requests (more) debugging.  May be repeated."
msgstr ""

#. type: =item
#: ../git-debrebase.1.pod:534
msgid "--experimntal-merge-resolution"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:536
msgid ""
"Enable experimental code for handling general merges (see L<git-debrebase(5)/"
"General merges>)."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:539
msgid ""
"This option may generate lossage of various kinds, including misleading "
"error messages, references to nonexistent documentation, and you being "
"handed an incomprehensible pile of multidimensional merge wreckage."
msgstr ""

#. type: =head1
#: ../git-debrebase.1.pod:547
msgid "UNSTITCHING AND LAUNDERING"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:549
msgid ""
"Several operations unstitch and launder the branch first.  In detail this "
"means:"
msgstr ""

#. type: =head2
#: ../git-debrebase.1.pod:552
msgid "Establish the current branch's ffq-prev"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:554
msgid ""
"If ffq-prev is not yet recorded, git-debrebase checks that the current "
"branch is ahead of relevant remote tracking branches.  The relevant branches "
"depend on the current branch (and its git configuration)  and are as follows:"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:566
msgid ""
"The branch that git would merge from (remote.<branch>.merge, remote.<branch>."
"remote);"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:571
msgid ""
"The branch git would push to, if different (remote.<branch>.pushRemote etc.);"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:576
msgid "For local dgit suite branches, the corresponding tracking remote;"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:581
msgid "If you are on C<master>, remotes/dgit/dgit/sid."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:586
msgid ""
"The apparently relevant ref names to check are filtered through branch."
"<branch>.ffq-ffrefs, which is a semicolon-separated list of glob patterns, "
"each optionally preceded by !; first match wins."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:591
msgid ""
"In each case it is a snag if the local HEAD is behind the checked remote, or "
"if local HEAD has diverged from it.  All the checks are done locally using "
"the remote tracking refs: git-debrebase does not fetch anything from "
"anywhere."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:597
msgid ""
"If these checks pass, or are forced, git-debrebse then records the current "
"tip as ffq-prev."
msgstr ""

#. type: =head2
#: ../git-debrebase.1.pod:601
msgid "Examine the branch"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:603
msgid ""
"git-debrebase analyses the current HEAD's history to find the anchor in its "
"breakwater, and the most recent breakwater tip."
msgstr ""

#. type: =head2
#: ../git-debrebase.1.pod:608
msgid "Rewrite the commits into laundered form"
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:610
msgid ""
"Mixed debian+upstream commits are split into two commits each.  Delta queue "
"(upstream files) commits bubble to the top.  Pseudomerges, and quilt patch "
"additions, are dropped."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:616
msgid ""
"This rewrite will always succeed, by construction.  The result is the "
"laundered branch."
msgstr ""

#. type: textblock
#: ../git-debrebase.1.pod:621
msgid "git-debrebase(1), dgit-maint-rebase(7), dgit(1), gitglossary(7)"
msgstr ""
